package com.pathashala57;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MineSweeperTest {

    @Test
    void continueGivenCellIsSafe() {
        Square aSquare = new Square(1, 1, true);
        Square anotherSquare = new Square(1, 2, false);
        List<Square> squares = new ArrayList<>();
        squares.add(aSquare);
        squares.add(anotherSquare);
        Grid grid = new Grid(squares);
        MineSweeper newGame = new MineSweeper(grid);
        assertFalse(newGame.openSquare(anotherSquare));
    }

    @Test
    void stopIfGivenCellHasMine() {
        Square aSquare = new Square(1, 1, true);
        Square anotherSquare = new Square(1, 2, false);
        List<Square> squares = new ArrayList<>();
        squares.add(aSquare);
        squares.add(anotherSquare);
        Grid grid = new Grid(squares);
        MineSweeper newGame = new MineSweeper(grid);
        assertTrue(newGame.openSquare(aSquare));
    }

    @Test
    void flagsGivenCell()
    {
        Square aSquare = new Square(1, 1, true);
        Square anotherSquare = new Square(1, 2, false);
        List<Square> squares = new ArrayList<>();
        squares.add(aSquare);
        squares.add(anotherSquare);
        Grid grid = new Grid(squares);
        MineSweeper newGame = new MineSweeper(grid);
        newGame.flag(aSquare);

    }

}
