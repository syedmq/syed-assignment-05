package com.pathashala57;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class GridTest {
    @Test
    void testCellExistsInAGrid()
    {
        Square aSquare = new Square(1, 1, true);
        Square anotherSquare = new Square(1, 2, true);
        List<Square> squares = new ArrayList<>();
        squares.add(aSquare);
        squares.add(anotherSquare);
        Grid grid=new Grid(squares);
        Assertions.assertTrue(grid.contains(aSquare));
    }
    @Test
    void givenCellHasAMineInAGrid()
    {
        Square aSquare =new Square(1,1,true);
        Square anotherSquare =new Square(1,2,true);
        List<Square> squares =new ArrayList<>();
        squares.add(aSquare);
        squares.add(anotherSquare);
        Grid grid=new Grid(squares);
        Assertions.assertTrue(grid.cellHasAMine(aSquare));
    }


}
