package com.pathashala57;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SquareTest {

    @Test
    void squareEqualsSameCell() {
        Square aSquare = new Square(1, 2, true);
        Assertions.assertEquals(aSquare, aSquare);
    }

    @Test
    void squareEqualsAnotherCellWithSameCoordinates() {
        Square aSquare = new Square(1, 2, true);
        Square anotherSquare = new Square(1, 2, false);
        Assertions.assertEquals(aSquare, anotherSquare);
    }

    @Test
    void squareNotEqualIfCoordinatesDifferent() {
        Square aSquare = new Square(1, 2, true);
        Square anotherSquare = new Square(1, 3, false);
        Assertions.assertFalse(aSquare.equals(anotherSquare));

    }

    @Test
    void squareNotEqualToObjectOfAnyDifferentClass() {
        Square aSquare = new Square(1, 2, true);
        Assertions.assertFalse(aSquare.equals(new Object()));

    }

    @Test
    void squareNotEqualToNull() {
        Square aSquare = new Square(1, 2, true);
        Assertions.assertFalse(aSquare.equals(null));

    }

    @Test
    void squareHasMine() {
        Square aSquare = new Square(1, 2, true);
        Assertions.assertTrue(aSquare.hasMine());

    }

    @Test
    void squareDoesNotHaveMine() {
        Square aSquare = new Square(1, 2, false);
        Assertions.assertFalse(aSquare.hasMine());

    }
    @Test
    void squareCanBeFlagged() {
        Square aSquare = new Square(1, 2, false);
        aSquare.flag();
        Assertions.assertTrue(aSquare.isFlagged());

    }

}
