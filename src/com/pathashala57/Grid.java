package com.pathashala57;

import java.util.List;

//Represents the layout of Cells
public class Grid {
    private List<Square> squares;

    public Grid(List<Square> squares) {

        this.squares = squares;
    }

    public boolean contains(Square aSquare) {
        return squares.contains(aSquare);
    }

    public boolean cellHasAMine(Square aSquare) {

        for (Square anotherSquare : squares) {
            if (aSquare.equals(anotherSquare))
                return anotherSquare.hasMine();

        }
        return false;
    }

    public void flag(Square aSquare) {
        for (Square anotherSquare : squares) {
            if (aSquare.equals(anotherSquare)) {
                anotherSquare.flag();
            }

        }

    }
}
