package com.pathashala57;

//represents a point on the grid
public class Square {

    private final int xCoordinate;
    private final int yCoordinate;
    private boolean hasMine;
    private boolean flag;


    public Square(int xCoordinate, int yCoordinate, boolean hasMine) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.hasMine = hasMine;
        flag=false;
    }

    @Override
    public boolean equals(Object that) {
        if (that == null) {
            return false;
        }
        if (that.getClass() != this.getClass()) {
            return false;
        }
        Square anotherSquare = (Square) that;

        return this.xCoordinate == anotherSquare.xCoordinate && this.yCoordinate == anotherSquare.yCoordinate;
    }

    public boolean hasMine() {
        return hasMine;
    }

    public void flag() {
        flag=true;
    }

    public boolean isFlagged() {
        return flag;
    }

}
