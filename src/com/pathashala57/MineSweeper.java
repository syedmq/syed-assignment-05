package com.pathashala57;

import com.pathashala57.Grid;

//Represents the game enviroment
public class MineSweeper {

    private Grid grid;

    public MineSweeper(Grid grid) {

        this.grid = grid;
    }

    public boolean openSquare(Square aSquare) {
        return grid.cellHasAMine(aSquare);
    }

    public void flag(Square aSquare) {
        grid.flag(aSquare);
    }
}
